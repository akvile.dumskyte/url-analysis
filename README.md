# URL Analysis

## Overview
This repository contains the data analysis project for Nord Security. The analysis is performed in a Jupyter notebook and is intended find hidden insights in the data, prepare dashboards and present the findings backed up by arguments of potential ways to improve the quality of the data highlighting potential bottlenecks and failure of services
.

## Contents
- `NS_Homework.ipynb`: The main Jupyter notebook containing all the analysis.
- `data`: were loded form given json files.

## How to Navigate This Repository
- Start by reading the `NS_Homework.ipynb` notebook which contains all the code and commentary for the analysis.

## Running the Analysis
To run the analysis, you will need Jupyter Notebook or JupyterLab installed on your machine:
- Install Jupyter: `pip install notebook`
- Launch Jupyter Notebook: `jupyter notebook`
- Navigate to the `NS_Homework.ipynb` file and open it.
